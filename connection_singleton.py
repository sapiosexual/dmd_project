__author__ = 'Admin'
from connection import Connection

class ConnectionSingleton(Connection):
    current_transaction = None

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = Connection.__new__(cls)
        return cls.instance
