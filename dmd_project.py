from builtins import print
from flask import Flask, session, render_template, redirect, url_for, request
from sqlalchemy import exc
import hashlib, traceback
from entities.Paper import Paper
from connection_singleton import ConnectionSingleton
from id_controller_singleton import IdControllerSingleton
import sql_templates

app = Flask(__name__)

app.config.update(dict(
    SECRET_KEY='kernel panic'
))


@app.route('/')
def main():
    if session.get('logged_in'):
        return render_template('inno/pages/main.html')
    else:
        return render_template('inno/pages/index.html')


# ========================References============================
@app.route('/register', methods=['GET', 'POST'])
def register():
    error = None
    if session.get('logged_in'):
        return redirect(url_for('main'))
    elif request.method == 'POST':
        connection = ConnectionSingleton()
        hasher = hashlib.md5()
        hasher.update(request.form['password'].encode('utf-8'))
        try:
            controller = IdControllerSingleton()
            connection.engine.execute("insert into \"user\" values (%s, %s, %s, %s)",
                                      request.form['login'],
                                      hasher.hexdigest(),
                                      request.form['type'],
                                      controller.increment_and_get_user_id(request.form['type']))
        except exc.IntegrityError:
            traceback.print_exc()
            error = 'user already exist'
        except:
            traceback.print_exc()
            error = 'something went wrong'
        if not error:
            return redirect(url_for('main'))
    else:
        return render_template('inno/pages/profile/register.html', error=error)


# TODO: Improve selects!!!
@app.route('/profile', methods=['GET', 'POST'])
def profile():
    if session.get('logged_in'):
        if session.get('type') == 'Reader':
            return render_template('inno/pages/profile/profile.html')
        else:
            papers = []
            connection = ConnectionSingleton()
            distinct = False;
            sorting_attribute = request.form['sorting_attribute'] if 'sorting_attribute' in request.form else 'year'
            order_template = process_sorting_attribute(sorting_attribute)
            papers_per_page = int(request.form['papers_per_page']) if 'papers_per_page' in request.form else 25
            page_number = int(request.form['page_number']) - 1 if 'page_number' in request.form else 0
            if session.get('type') == 'Publisher':
                entries = connection.engine.execute(sql_templates.search_template_result(distinct) +
                                                    sql_templates.search_by_publisher_user() +
                                                    order_template +
                                                    sql_templates.offset_template(),
                                                    session.get('login'),
                                                    papers_per_page,
                                                    page_number * papers_per_page)
                countEntry = connection.engine.execute(sql_templates.search_template_count(distinct) +
                                                       sql_templates.search_by_publisher_user(),
                                                       session.get('login'))
            elif session.get('type') == 'Author':
                entries = connection.engine.execute(sql_templates.search_template_result(distinct) +
                                                    sql_templates.search_by_author_user() +
                                                    order_template +
                                                    sql_templates.offset_template(),
                                                    session.get('login'),
                                                    papers_per_page,
                                                    page_number * papers_per_page)
                countEntry = connection.engine.execute(sql_templates.search_template_count(distinct) +
                                                       sql_templates.search_by_author_user(),
                                                       session.get('login'))
            for entry in entries.fetchall():
                paper = Paper(entry)
                paper.load_authors_from_db()
                papers.append(paper)
            return render_template('inno/pages/profile/profile.html',
                                   papers=papers,
                                   count=countEntry.first()['count'],
                                   papers_per_page=papers_per_page,
                                   page_number=page_number)
    else:
        return render_template('inno/pages/login.html')


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if session.get('logged_in'):
        return redirect(url_for('main'))
    elif request.method == 'POST':
        connection = ConnectionSingleton()
        hasher = hashlib.md5()
        hasher.update(request.form['password'].encode('utf-8'))
        cur = connection.engine.execute('SELECT * FROM "user" WHERE login = %s', request.form['login'])
        row = cur.first()
        if row:
            if row['hash'] == hasher.hexdigest():
                session['logged_in'] = True
                session['type'] = row['type']
                session['user_id'] = row['internal_id']
                session['login'] = request.form['login']
                return render_template('inno/pages/main.html')
            else:
                error = 'Wrong password'
        else:
            error = 'Wrong username'
    return render_template('inno/pages/login.html', error=error)


@app.route('/base')
def base():
    return render_template('inno/pages/basic_templates/base.html')


@app.route('/basic_search', methods=['GET', 'POST'])
def basic_search():
    papers = []
    if request.method == 'POST':
        print(request.form)
        connection = ConnectionSingleton()
        distinct = False
        query = request.form['query']
        sorting_attribute = request.form['sorting_attribute'] if 'sorting_attribute' in request.form else 'year'
        search_type = request.form['search_type'] if 'search_type' in request.form else 'title'
        search_template = process_search_type(search_type, query)
        order_template = process_sorting_attribute(sorting_attribute)
        papers_per_page = int(request.form['papers_per_page']) if 'papers_per_page' in request.form else 25
        page_number = int(request.form['page_number']) - 1 if 'page_number' in request.form else 0
        cur = connection.engine.execute(search_template[0] +
                                        order_template +
                                        sql_templates.offset_template(),
                                        search_template[2],
                                        papers_per_page,
                                        page_number * papers_per_page)
        for entry in cur.fetchall():
            paper = Paper(entry)
            paper.load_authors_from_db()
            papers.append(paper)
        cur = connection.engine.execute(search_template[1],
                                        search_template[2])
        return render_template('inno/pages/search/search_result.html',
                               papers=papers, count=cur.first()['count'], papers_per_page=papers_per_page,
                               page_number=page_number + 1, query=query,  sorting_attribute=sorting_attribute,
                               search_type=search_type)
    else:
        return render_template('inno/pages/search/basic_search_empty.html', sorting_attribute='year', search_type='title')


def process_sorting_attribute(sorting_attribute):
    if sorting_attribute == 'year':
        return sql_templates.order_by_year()
    else:
        return sql_templates.order_by_title()


def process_search_type(search_type, query):
    res = []
    distinct = False
    unstrict = False
    if search_type == 'title':
        unstrict = True
        search_template = sql_templates.search_by_title()
    elif search_type == 'author':
        distinct = True
        unstrict = True
        search_template = sql_templates.search_by_author()
    elif search_type == 'year':
        if not query.isdigit():
            query = 0
        search_template = sql_templates.search_by_year()
    elif search_type == 'research_area':
        unstrict = True
        search_template = sql_templates.search_by_research_area()
    elif search_type == 'affiliation':
        distinct = True
        unstrict = True
        search_template = sql_templates.search_by_affiliation()
    else:
        distinct = True
        unstrict = True
        search_template = sql_templates.search_by_key_word()
    if unstrict:
        query = "%" + query + "%"
    res.append(sql_templates.search_template_result(distinct) + search_template)
    res.append(sql_templates.search_template_count(distinct) + search_template)
    res.append(query)
    return res


@app.route('/first_search')
def first_search():
    return render_template('inno/pages/search/basic_search_empty.html')


@app.route('/logout')
def logout():
    session['logged_in'] = None
    session['type'] = None
    session['user_id'] = None
    session['login'] = None
    return redirect(url_for('main'))


# =========================Just for tests===================

@app.route('/paper_details/<p_id>', methods=['GET', 'POST'])
def paper_details(p_id):
    if session.get('logged_in'):
        connection = ConnectionSingleton()
        cur = connection.engine.execute(sql_templates.paper_details_template(), p_id)
        row = cur.first()
        if row:
            paper = Paper(row)
            paper.load_authors_from_db()
            paper.load_key_words_from_db()
            paper.load_affiliations_from_db()
            return render_template('inno/pages/publication/update_paper.html', paper=paper)
        else:
            return redirect(url_for('main'))
            # TODO:Find better way to get paper
    else:
        return render_template('inno/pages/login.html')


@app.route('/add_paper', methods=['GET', 'POST'])
def add_paper():
    error = None
    trans = ConnectionSingleton().engine.contextual_connect().begin()
    if request.method == 'POST':
        try:
            publisher_id = process_publisher(request.form)
            publication_id = process_publication(request.form, publisher_id)
            research_area_id = process_research_area(request.form)
            paper_id = process_paper(request.form, publication_id, research_area_id)

            if paper_id:
                process_key_words(request.form, paper_id)
                process_authors(request.form, paper_id)
                process_affiliations(request.form, paper_id)
                trans.commit()
                return redirect(url_for('paper_details', p_id=paper_id))
            else:
                trans.rollback()
                error = 'Something went wrong'
        except:
            trans.rollback()
            traceback.print_exc()
            error = 'transaction failed'
    return render_template('inno/pages/publication/add_paper.html', error=error)


@app.route('/update_paper', methods=['GET', 'POST'])
def update_paper():
    trans = ConnectionSingleton().engine.contextual_connect().begin()
    if request.method == 'POST':
        try:
            print(request.form)
            paper_id = request.form['id']
            if request.form['tab'] == 'main':
                paper_id = process_main(request.form, paper_id)
                print()
            elif request.form['tab'] == 'abstract':
                process_abstract(request.form, paper_id)
            elif request.form['tab'] == 'authors':
                process_authors_update(request.form, paper_id)
            else:
                process_key_words_update(request.form, paper_id)
            if not paper_id:
                trans.rollback()
            else:
                trans.commit()
            return redirect(url_for('paper_details', p_id=paper_id))
        except:
            trans.rollback()
            traceback.print_exc()
    else:
        return redirect(url_for('main'))


def process_abstract(dic, paper_id):
    ConnectionSingleton().engine.execute(sql_templates.update_abstract(), dic['abstract'], paper_id)


def process_authors_update(dic, paper_id):
    ConnectionSingleton().engine.execute(sql_templates.delete_paper_authors(), paper_id)
    process_authors(dic, paper_id)


def process_key_words_update(dic, paper_id):
    ConnectionSingleton().engine.execute(sql_templates.delete_paper_key_words(), paper_id)
    process_key_words(dic, paper_id)


def process_main(dic, paper_id):
    publisher_id = process_publisher(dic)
    publication_id = process_publication(dic, publisher_id)
    research_area_id = process_research_area(dic)
    paper_id = process_paper_update(dic, research_area_id, publication_id, paper_id)
    if paper_id:
        ConnectionSingleton().engine.execute(sql_templates.delete_paper_affiliations(), paper_id)
        process_affiliations(dic, paper_id)
    return paper_id


def process_paper_update(dic, research_area_id, publication_id, paper_id):
    if not dic['title'] and not dic['link'] and not dic['abstract'] and not dic['doi'] \
            and not dic['spage'] and not dic['fpage'] and not research_area_id and not publication_id:
        paper_id = None
    else:
        ConnectionSingleton().engine.execute(sql_templates.update_paper(), dic['title'], dic['link'], dic['doi'],
                                             research_area_id, publication_id, dic['spage'], dic['fpage'], paper_id)

    return paper_id


@app.route('/browse', methods=['GET', 'POST'])
def browse():
    connection = ConnectionSingleton()
    papers = []
    distinct = False
    print(request.form)
    sorting_attribute = request.form['sorting_attribute'] if 'sorting_attribute' in request.form else 'year'
    order_template = process_sorting_attribute(sorting_attribute)
    papers_per_page = int(request.form['papers_per_page']) if 'papers_per_page' in request.form else 25
    page_number = int(request.form['page_number']) - 1 if 'page_number' in request.form else 0
    cur = connection.engine.execute(sql_templates.search_template_result(distinct) +
                                    order_template +
                                    sql_templates.offset_template(),
                                    papers_per_page,
                                    page_number * papers_per_page)
    for entry in cur.fetchall():
        paper = Paper(entry)
        paper.load_authors_from_db()
        papers.append(paper)

    cur = connection.engine.execute(sql_templates.search_template_count(distinct))
    return render_template('inno/pages/browse/browse.html', papers=papers, count=cur.first()['count'],
                           papers_per_page=papers_per_page, page_number=page_number + 1,
                           sorting_attribute=sorting_attribute)


@app.route('/delete_paper', methods=['GET', 'POST'])
def delete_paper():
    if request.method == 'POST':
        connection = ConnectionSingleton()
        trans = connection.engine.contextual_connect().begin()
        paper_id = request.form['id']
        try:
            connection.engine.execute(sql_templates.delete_paper_affiliations(), paper_id)
            connection.engine.execute(sql_templates.delete_paper_key_words(), paper_id)
            connection.engine.execute(sql_templates.delete_paper_authors(), paper_id)
            connection.engine.execute(sql_templates.delete_paper(), paper_id)
            trans.commit()
        except:
            traceback.print_exc()
            trans.rollback()
    return redirect(url_for('main'))


def process_publisher(dic):
    connection = ConnectionSingleton()
    id_controller = IdControllerSingleton()

    publisher = dic['publisher_name']
    if not publisher:
        publisher_id = None
    else:
        cur = connection.engine.execute(sql_templates.check_publisher(), publisher)
        row = cur.first()
        if row is None:
            publisher_id = id_controller.increment_and_get_publisher_id()
            connection.engine.execute(sql_templates.insert_publisher(), publisher_id, publisher)
        else:
            publisher_id = row['id']

    return publisher_id


def process_publication(dic, publisher_id):
    connection = ConnectionSingleton()
    id_controller = IdControllerSingleton()

    if not dic['publication_name'] and not dic['volume'] and not dic['issue'] and not dic['issn'] \
            and not dic['isbn'] and not dic['year'] and not publisher_id:
        publication_id = None
    else:
        cur = connection.engine.execute(sql_templates.check_publication(), dic['publication_name'], dic['volume'],
                                        dic['issue'], dic['issn'], dic['isbn'], publisher_id, dic['publication_type'],
                                        dic['year'])
        row = cur.first()
        if row is None:
            publication_id = id_controller.increment_and_get_publication_id()
            connection.engine.execute(sql_templates.insert_publication(), publication_id, dic['issn'],
                                      dic['publication_name'], dic['year'], publisher_id, dic['isbn'],
                                      dic['publication_type'], dic['volume'], dic['issue'])
        else:
            publication_id = row['id']

    return publication_id


def process_research_area(dic):
    connection = ConnectionSingleton()
    id_controller = IdControllerSingleton()

    research_area = dic['research_area']
    if not research_area:
        research_area_id = None
    else:
        cur = connection.engine.execute(sql_templates.check_research_area(), research_area)
        row = cur.first()
        if row is None:
            research_area_id = id_controller.increment_and_get_research_area_id()
            connection.engine.execute(sql_templates.insert_research_area(), research_area_id, research_area)
        else:
            research_area_id = row['id']

    return research_area_id


def process_paper(dic, publication_id, research_area_id):
    if not dic['title'] and not dic['link'] and not dic['abstract'] and not dic['doi'] \
            and not dic['spage'] and not dic['fpage'] and not research_area_id and not publication_id:
        paper_id = None
    else:
        connection = ConnectionSingleton()
        id_controller = IdControllerSingleton()
        paper_id = id_controller.increment_and_get_paper_id()
        connection.engine.execute(sql_templates.insert_paper(), paper_id, dic['title'], dic['link'], dic['abstract'],
                                  dic['doi'], research_area_id, publication_id, dic['spage'], dic['fpage'])

    return paper_id


def process_key_words(dic, paper_id):
    connection = ConnectionSingleton()
    id_controller = IdControllerSingleton()
    pattern = 'Keyword'
    number = 1
    key = pattern + str(number)
    key_words = []
    while key in dic:
        if dic[key]:
            key_word = dic[key].lower()
            key_words.append(key_word.strip())
        number += 1
        key = pattern + str(number)
    if key_words.__len__():
        args = '\'' + '\', \''.join(key_words) + '\''
        cur = connection.engine.execute(sql_templates.check_key_words().replace('%s', args))
        existed_key_words = dict()
        for entry in cur.fetchall():
            existed_key_words[entry['term']] = entry['id']
            if entry['term'] in key_words:  # TODO delete
                key_words.remove(entry['term'])
        for key_word in key_words:
            key_word_id = id_controller.increment_and_get_key_word_id()
            connection.engine.execute(sql_templates.insert_key_word(), key_word_id, key_word)
            existed_key_words[key_word] = key_word_id
        for key_word_id in existed_key_words.values():
            connection.engine.execute(sql_templates.insert_paper_key_word(), paper_id, key_word_id)


def process_authors(dic, paper_id):
    connection = ConnectionSingleton()
    id_controller = IdControllerSingleton()
    pattern = 'Author'
    number = 1
    key = pattern + str(number)
    authors = []
    authors_order = dict()
    while key in dic:
        if dic[key]:
            author = dic[key].strip()
            authors.append(author)
            authors_order[author] = number if author not in authors_order else authors_order[author]
        number += 1
        key = pattern + str(number)
    if authors.__len__():
        args = '\'' + '\', \''.join(authors) + '\''
        cur = connection.engine.execute(sql_templates.check_authors().replace('%s', args))
        existed_authors = dict()
        for entry in cur.fetchall():
            existed_authors[entry['name']] = entry['id']
            authors.remove(entry['name'])
        for author in authors:
            author_id = id_controller.increment_and_get_author_id()
            connection.engine.execute(sql_templates.insert_author(), author_id, author)
            existed_authors[author] = author_id
        for author in existed_authors.items():
            connection.engine.execute(sql_templates.insert_paper_author(), paper_id, author[1],
                                      authors_order[author[0]])


def process_affiliations(dic, paper_id):
    connection = ConnectionSingleton()
    id_controller = IdControllerSingleton()
    pattern = 'Affiliation'
    number = 1
    key = pattern + str(number)
    affiliations = []
    while key in dic:
        if dic[key]:
            affiliation = dic[key]
            affiliations.append(affiliation.strip())
        number += 1
        key = pattern + str(number)
    if affiliations.__len__():
        args = '\'' + '\', \''.join(affiliations) + '\''
        cur = connection.engine.execute(sql_templates.check_affiliations().replace('%s', args))
        existed_affiliations = dict()
        for entry in cur.fetchall():
            existed_affiliations[entry['name']] = entry['id']
            affiliations.remove(entry['name'])
        for affiliation in affiliations:
            affiliation_id = id_controller.increment_and_get_affiliation_id()
            connection.engine.execute(sql_templates.insert_affiliation(), affiliation_id, affiliation)
            existed_affiliations[affiliation] = affiliation_id
        for affiliation_id in existed_affiliations.values():
            connection.engine.execute(sql_templates.insert_paper_affiliation(), paper_id, affiliation_id)


def if_exists(var):
    if var == 'None' or var == '-1':
        return False
    else:
        return True


# id = request.args.get('p_id', '')
#     author1 = request.args.get('author1', '')
#     print(request.form)
# papers = []
# connection = ConnectionSingleton()
#     UPDATE author
#     SET ContactName='Alfred Schmidt', City='Hamburg'
#     WHERE CustomerName='Alfreds Futterkiste';

# =========================End of tests======================


if __name__ == '__main__':
    app.run()
