__author__ = 'Admin'


class KeyWord:
    def __init__(self, db_entry):
        self.id = db_entry['id'] if 'id' in db_entry else -1
        self.term = db_entry['term'] if 'term' in db_entry else ''
