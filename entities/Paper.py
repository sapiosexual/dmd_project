__author__ = 'Admin'
from connection_singleton import ConnectionSingleton
from entities.Author import Author
from entities.KeyWord import KeyWord
from entities.Affiliation import Affiliation


class Paper:
    def __init__(self, db_entry):
        self.id = db_entry['id'] if 'id' in db_entry else None
        self.title = db_entry['title'].replace('<BR>', ' ') if 'title' in db_entry else None
        self.link = db_entry['link'] if 'link' in db_entry else None
        self.abstract = db_entry['abstract'].replace('<BR>', '\r\n') if 'abstract' in db_entry else None
        self.type = db_entry['pub_type'] if 'pub_type' in db_entry else None
        self.doi = db_entry['doi'] if 'doi' in db_entry else None
        self.spage = db_entry['spage'] if 'spage' in db_entry else None
        self.fpage = db_entry['fpage'] if 'fpage' in db_entry else None
        self.research_area_id = db_entry['research_area_id'] if 'research_area_id' in db_entry else None
        self.research_area = db_entry['research_area'] if 'research_area' in db_entry else None
        self.publication_id = db_entry['publication_id'] if 'publication_id' in db_entry else None
        self.isbn = db_entry['isbn'] if 'isbn' in db_entry else None
        self.issn = db_entry['issn'] if 'issn' in db_entry else None
        self.publication_name = db_entry['publication_name'] if 'publication_name' in db_entry else None
        self.issue = db_entry['issue'] if 'issue' in db_entry else None
        self.volume = db_entry['volume'] if 'volume' in db_entry else None
        self.year = db_entry['year'] if 'year' in db_entry else None
        self.publisher_id = db_entry['publisher_id'] if 'publisher_id' in db_entry else None
        self.publisher_name = db_entry['publisher_name'] if 'publisher_name' in db_entry else None
        self.authors = None
        self.key_words = None
        self.affiliations = None

    def load_authors_from_db(self):
        connection = ConnectionSingleton()
        authors = []
        cur = connection.engine.execute('''select a.* from paper_authors pa
                                           join author a on a.id = pa.author_id
                                           where pa.paper_id = %s
                                           order by pa.position''', self.id)
        for entry in cur.fetchall():
            authors.append(Author(entry))
        self.authors = authors

    def load_key_words_from_db(self):
        connection = ConnectionSingleton()
        key_words = []
        cur = connection.engine.execute('''select kw.* from paper_key_words pkw
                                           join key_word kw on kw.id = pkw.key_word_id
                                           where pkw.paper_id = %s;''', self.id)
        for entry in cur.fetchall():
            key_words.append(KeyWord(entry))
        self.key_words = key_words

    def load_affiliations_from_db(self):
        connection = ConnectionSingleton()
        affiliations = []
        cur = connection.engine.execute('''select a.* from paper_affiliations pa
                                           join affiliation a on a.id = pa.affiliation_id
                                           where pa.paper_id = %s;''', self.id)
        for entry in cur.fetchall():
            affiliations.append(Affiliation(entry))
        self.affiliations = affiliations
