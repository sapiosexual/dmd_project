__author__ = 'Admin'
from connection_singleton import ConnectionSingleton
import sql_templates


class IdController:
    user_types = dict(
        Publisher=0,
        Author=0,
        Reader=0
    )

    def __init__(self):
        connection = ConnectionSingleton()
        for key in self.user_types.keys():
            cur = connection.engine.execute(sql_templates.user_id(), key)
            row = cur.first()
            self.user_types[key] = row[0]
        cur = connection.engine.execute(sql_templates.publisher_id())
        self.publisher_id = cur.first()[0]
        cur = connection.engine.execute(sql_templates.publication_id())
        self.publication_id = cur.first()[0]
        cur = connection.engine.execute(sql_templates.research_area_id())
        self.research_area_id = cur.first()[0]
        cur = connection.engine.execute(sql_templates.paper_id())
        self.paper_id = cur.first()[0]
        cur = connection.engine.execute(sql_templates.key_word_id())
        self.key_word_id = cur.first()[0]
        cur = connection.engine.execute(sql_templates.author_id())
        self.author_id = cur.first()[0]
        cur = connection.engine.execute(sql_templates.affiliation_id())
        self.affiliation_id = cur.first()[0]

    def increment_and_get_user_id(self, key):
        value = self.user_types.get(key)
        self.user_types[key] = value + 1
        return value

    def increment_and_get_publisher_id(self):
        value = self.publisher_id
        self.publisher_id = value + 1
        return value

    def increment_and_get_publication_id(self):
        value = self.publication_id
        self.publication_id = value + 1
        return value

    def increment_and_get_research_area_id(self):
        value = self.research_area_id
        self.research_area_id = value + 1
        return value

    def increment_and_get_paper_id(self):
        value = self.paper_id
        self.paper_id = value + 1
        return value

    def increment_and_get_key_word_id(self):
        value = self.key_word_id
        self.key_word_id = value + 1
        return value

    def increment_and_get_author_id(self):
        value = self.author_id
        self.author_id = value + 1
        return value

    def increment_and_get_affiliation_id(self):
        value = self.affiliation_id
        self.affiliation_id = value + 1
        return value
