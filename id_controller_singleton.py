__author__ = 'Admin'
from id_controller import IdController


class IdControllerSingleton(IdController):
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = IdController.__new__(cls)
        return cls.instance
