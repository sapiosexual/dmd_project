__author__ = 'Admin'


# ===================Search templates================================
def search_template_result(distinct):
    if distinct:
        result = 'SELECT DISTINCT '
    else:
        result = 'SELECT '
    return result + '''p.id, p.title, p.doi, p.spage, p.fpage,
                       pbln.issue, pbln.volume, pbln.year FROM paper p
           LEFT JOIN publication pbln ON pbln.id = p.publication_id'''


def search_template_count(distinct):
    if distinct:
        result = 'SELECT DISTINCT '
    else:
        result = 'SELECT '
    return result + '''COUNT(p.id) AS count FROM paper p
           LEFT JOIN publication pbln ON pbln.id = p.publication_id'''


def paper_details_template():
    return '''SELECT p.id, p.title, p.link, p.abstract, p.doi, p.spage, p.fpage,
              ra.id AS research_area_id, ra.area AS research_area,
              pbln.id AS publication_id, pbln.isbn, pbln.issn,
              pbln.type AS pub_type, pbln.name AS publication_name, pbln.issue,
              pbln.volume, pbln.year, pblr.id AS publisher_id,
              pblr.name AS publisher_name FROM paper p
              LEFT JOIN research_area ra ON ra.id = p.research_area_id
              LEFT JOIN publication pbln ON pbln.id = p.publication_id
              LEFT JOIN publisher pblr ON pbln.publisher_id = pblr.id
              WHERE p.id = (%s);'''


def search_by_research_area():
    return '''\nLEFT JOIN research_area ra ON ra.id = p.research_area_id
                WHERE lower(ra.area) LIKE lower(%s)'''


def search_by_author():
    return '''\nLEFT JOIN paper_authors pa ON pa.paper_id = p.id
                LEFT JOIN author a ON a.id = pa.author_id
                WHERE lower(a.name) LIKE lower(%s)'''


def search_by_year():
    return '''\nWHERE pbln.year = %s'''


def search_by_venue():
    return '''\nWHERE lower(pbln.name) LIKE lower(%s)'''


def search_by_title():
    return '''\nWHERE lower(p.title) LIKE lower(%s)'''


def search_by_key_word():
    return '''\nLEFT JOIN paper_key_words pkw ON pkw.paper_id = p.id
                LEFT JOIN key_word kw ON kw.id = pkw.key_word_id
                WHERE lower(kw.term) LIKE lower(%s)'''


def search_by_type():
    return '''\nWHERE pbln.type = %s'''


def search_by_affiliation():
    return '''\nLEFT JOIN paper_affiliations pa ON pa.paper_id = p.id
                LEFT JOIN affiliation a ON a.id = pa.affiliation_id
                WHERE lower(a.name) LIKE lower(%s)'''


def search_by_related_articles():
    return '''\nLEFT JOIN "similar" s ON s.to_paper_id = p.id
                WHERE s.to_paper_id = %s'''


def search_by_author_user():
    return '''\nLEFT JOIN paper_authors pa ON pa.paper_id = p.id
                LEFT JOIN "user" u ON u.internal_id = pa.author_id
                WHERE u.login = %s'''


def search_by_publisher_user():
    return '''\nLEFT JOIN publisher pblr ON pbln.publisher_id = pblr.id
                LEFT JOIN "user" u ON u.internal_id = pblr.id
                WHERE u.login = %s'''


def order_by_year():
    return '''\nORDER BY pbln.year'''


def order_by_title():
    return '''\nORDER BY p.title'''


def offset_template():
    return '''\nLIMIT %s
              \nOFFSET %s;'''
# ===================Search templates end================================


# ===================Paper addition templates============================
def check_publisher():
    return '''SELECT id FROM publisher
              WHERE name = %s'''


def insert_publisher():
    return '''INSERT INTO publisher VALUES (%s, %s);'''


def check_publication():
    return '''SELECT * FROM publication
              WHERE name = %s AND volume = %s AND issue = %s AND issn = %s AND
                    isbn = %s AND publisher_id = %s AND type = %s AND year = %s;'''


def insert_publication():
    return '''INSERT INTO publication VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s);'''


def check_research_area():
    return '''SELECT id FROM research_area
              WHERE area = %s'''


def insert_research_area():
    return '''INSERT INTO research_area VALUES (%s, %s);'''


def insert_paper():
    return '''INSERT INTO paper VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s);'''


def update_paper():
    return '''UPDATE paper SET title = %s,
                               link = %s,
                               doi = %s,
                               research_area_id = %s,
                               publication_id = %s,
                               spage = %s,
                               fpage = %s
                           WHERE id = %s;'''


def update_abstract():
    return '''UPDATE paper SET abstract = %s
                           WHERE id = %s;'''


def delete_paper_affiliations():
    return '''DELETE FROM paper_affiliations WHERE paper_id = %s;'''


def delete_paper_authors():
    return '''DELETE FROM paper_authors WHERE paper_id = %s;'''


def delete_paper_key_words():
    return '''DELETE FROM paper_key_words WHERE paper_id = %s;'''


def delete_paper():
    return '''DELETE FROM paper WHERE id = %s;'''


def check_key_words():
    return '''SELECT * FROM key_word
              WHERE term IN (%s)'''


def insert_key_word():
    return '''INSERT INTO key_word VALUES (%s, %s);'''


def insert_paper_key_word():
    return '''INSERT INTO paper_key_words VALUES (%s, %s);'''


def check_authors():
    return '''SELECT * FROM author
              WHERE name IN (%s)'''


def insert_author():
    return '''INSERT INTO author VALUES (%s, %s);'''


def insert_paper_author():
    return '''INSERT INTO paper_authors VALUES (%s, %s, %s);'''


def check_affiliations():
    return '''SELECT * FROM affiliation
              WHERE name IN (%s)'''


def insert_affiliation():
    return '''INSERT INTO affiliation VALUES (%s, %s);'''


def insert_paper_affiliation():
    return '''INSERT INTO paper_affiliations VALUES (%s, %s);'''
# ===================Paper addition templates end========================


# ===================ID controller templates=============================
def user_id():
    return 'SELECT max(internal_id) + 1 FROM "user" WHERE type=%s'


def publisher_id():
    return 'SELECT max(id) + 1 FROM publisher'


def publication_id():
    return 'SELECT max(id) + 1 FROM publication'


def research_area_id():
    return 'SELECT max(id) + 1 FROM research_area'


def paper_id():
    return 'SELECT max(id) + 1 FROM paper'


def key_word_id():
    return 'SELECT max(id) + 1 FROM key_word'


def author_id():
    return 'SELECT max(id) + 1 FROM author'


def affiliation_id():
    return 'SELECT max(id) + 1 FROM affiliation'
# ===================ID controller templates end=========================
